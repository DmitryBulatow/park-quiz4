package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.time.LocalDate;

public class Park2 {

	//Вносить изменения в этот класс не нужно!

	private String title;

	private String totalSquare;

	@FieldName("title")
	private String legalName;

	@NotBlank
	@MaxLength(13)
	private String ownerOrganizationInn;

	@NotBlank
	private LocalDate foundationYear;

	private Park2() {
	}

	@Override
	public String toString() {
		return "Park{" +
				"legalName='" + legalName + '\'' +
				", ownerOrganizationInn='" + ownerOrganizationInn + '\'' +
				", foundationYear=" + foundationYear +
				'}';
	}
}
