package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.*;

public class ParkParsingServiceImpl implements ParkParsingService {

	//Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
	@Override
	public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
		Class clazz = null;
		Park park = null;
		try {
			clazz = Class.forName(Park.class.getName());
			Constructor declaredConstructor = clazz.getDeclaredConstructor();
			declaredConstructor.setAccessible(true);
			park = (Park) declaredConstructor.newInstance();
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
			e.printStackTrace();
		}

		InputStream in;
		Map<Field, String> info = new HashMap<>();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(
						new FileInputStream(new File(parkDatafilePath))))) {
			br.readLine();

			for (int i = 0; i < 5; i++) {
				String[] split = br.readLine().trim().replace("\"", "").split(":");
				Field[] declaredFielddds = new Field[0];
				try {
					declaredFielddds = Class.forName(Park2.class.getName()).getDeclaredFields();
				} catch (ClassNotFoundException e) {
					continue;
				}
				Field declaredField = null;
				for (int j = 0; j < declaredFielddds.length; j++) {
					if (declaredFielddds[j].getName().equals(split[0])){
						declaredField = declaredFielddds[j];
						declaredField.setAccessible(true);
						info.put(declaredField, split[1].replace("\"", ""));
					}
				}

			}


		} catch (IOException e) {
			e.printStackTrace();
		}

		info.keySet()
				.forEach(field -> {
					if (info.get(field).equals("null")) {
						info.put(field, null);
					}
				});

		List<ParkParsingException.ParkValidationError> exceptionsList = new ArrayList<>();
		for (Field field : clazz.getDeclaredFields()) {
			field.setAccessible(true);
			if (field.getAnnotations() != null) {

				if (Objects.nonNull(field.getAnnotation(NotBlank.class))) {
					if (info.get(field) == null || info.get(field).equals("")) {
						exceptionsList.add(new ParkParsingException.
								ParkValidationError(field.getName(), " null or empty "));
					} else {
						try {
							if (field.getName().equals("foundationYear")) {
								field.set(park, LocalDate.parse(info.get(field)));
							} else {
								field.set(park, info.get(field));
							}
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}

				if (Objects.nonNull(field.getAnnotation(MaxLength.class))) {
					if (info.get(field).length() > field.getAnnotation(MaxLength.class).value()) {
						exceptionsList.add(new ParkParsingException.
								ParkValidationError(field.getName(), " size more them MAX"));
					} else {
						try {
							if (field.getName().equals("foundationYear")) {
								field.set(park, LocalDate.parse(info.get(field)));
							} else {
								field.set(park, info.get(field));
							}
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}

				if (Objects.nonNull(field.getAnnotation(FieldName.class))) {

					String value = field.getAnnotation(FieldName.class).value();

					try {
						if (value.equals("foundationYear")) {
							field.set(park, LocalDate.parse(info.get(value)));
						} else {
							field.set(park, info.get(value));
						}
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}

			}
		}

		if (exceptionsList.size()>0){
			throw new ParkParsingException("ERROR MEN", exceptionsList);
		}

		//write your code here yea @meh
		return park;
	}
}
